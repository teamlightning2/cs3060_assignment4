/*
 File Header
 Name: Jason Patterson & Daniel Bates
 Class: CS 3060-001
 Program: Assignment 4
 
 The more1 program displays the contents of a file to the terminal one screenful at a time.
 */

#include<stdio.h>
#include<termios.h>
#include<unistd.h>
#include<signal.h>
#include<fcntl.h>
#include<stdlib.h>
#include<sys/stat.h>

#define LINE_LENGTH (512)
#define LINES_ON_SCREEN (23)
#define PERCENT_CONVERSION (100)

/*
 Name: turnTermFlagsOff
 Desc: A method to turn off the Echo and Canonical mode for the terminal.
 Rtrn: 0 if succsessful and -1 if error
 Args: A file descriptor
 */
int turnTermFlagsOff(int);

/*
 Name: turnTermFlagsOn
 Desc: A method to turn on the Echo and Canonical mode for the terminal.
 Rtrn: 0 if succsessful and -1 if error
 Args: A file descriptor
 */
int turnTermFlagsOn(int);

/*
 Name: displayToScreen
 Desc: A method to display a certain number of lines to the screen from a file
 Rtrn: None
 Args:  fp: a pointer to the file to be displayed
        NumOfLines: the number of lines to be displayed to the screen
        path: the path to the file being opened
 */
void displayToScreen(FILE *, int, char *);

/*
 Name: getMore
 Desc: A method to prompt the user to continue or quit
 Rtrn: an int (the number value of the users response)
 Args: flag: a flag for if stdin or a file
 */
int getMore(char *, int, int, int);


int countBytes(char *);

void SIGhandler(int);

int main(int argc, char *argv[])
{
    signal(SIGINT, SIGhandler);
    int flag; //0 for stdin and 1 for file
    FILE *fp;
    if (argc == 1) {
        //No options given
        flag = 0;
        displayToScreen(stdin, flag, argv[0]);
    }else if(argc == 2){
        //One option givin
        flag = 1;
        if ((fp = fopen(*++argv, "r")) != NULL) {
            displayToScreen(fp, flag, *argv);
            fclose(fp);
        }
        
    }else{
        perror("Too many arguments.");
    }
    return 0;
}

void displayToScreen(FILE *fp, int flag, char *path)
{
    int numOfLines = LINES_ON_SCREEN;
    char line[LINE_LENGTH];
    int reply;
    struct stat statStruct;
    int bytesPrinted = 0;
    int bytesInFile = 0;
    int nameFlag = 1;
    //int getMore();
    if (flag == 1) {
        stat(path, &statStruct);
        bytesInFile = (int)statStruct.st_size;
        printf("%d", bytesInFile);
    }
    
    while (fgets(line, LINE_LENGTH, fp)){
        bytesPrinted += countBytes(line);
        fputs(line, stdout);
        if (numOfLines == 0){
            reply = getMore(path, bytesPrinted, bytesInFile, nameFlag);
            nameFlag = 0;
            if(reply == 0){
                break;
            }
            numOfLines += reply; 
        } 
        numOfLines--;
    }
}

int getMore(char * path, int bP, int bInF, int nameFlag)
{
    int response;
    int fd_tty;
    FILE *fp_tty;
    if ((fd_tty = open("/dev/tty", O_RDWR)) == -1) {
        perror("tty open error");
        exit(1);
    }
    if ((fp_tty = fdopen(fd_tty, "r+")) == NULL) {
        perror("tty fp open error");
        exit(1);
    }
    
    // to display either file name and percent or #of bytes
    if (bInF == 0){
        printf("\033[7mBytes (%d)\033[m", bP);
    }else{
        if (nameFlag)
        {
            printf("\033[7m%s %d%%\033[m", path, (bP * PERCENT_CONVERSION / bInF));
        }else{
            printf("\033[7m%d%%\033[m", (bP * PERCENT_CONVERSION / bInF));
        }
    }
    
	if ((turnTermFlagsOff(fd_tty)) == -1){
		perror("tcsetattr error");
    }else{
		while((response = fgetc(fp_tty)) != EOF)
        {
        	if(response == 'q'){
                if ((turnTermFlagsOn(fd_tty)) == -1){
                    perror("tcsetattr error");
                }else{
                    close(fd_tty);
                    fclose(fp_tty);
                    printf("\33[2K\r");
                    printf("\n");
               		return 0;
                }
           	}
          	if(response == ' '){
                if ((turnTermFlagsOn(fd_tty)) == -1){
                    perror("tcsetattr error");
                }else{
                    close(fd_tty);
                    fclose(fp_tty);
                    printf("\33[2K\r");
                	return LINES_ON_SCREEN;
                }
            }
            if(response == '\n'){
                if ((turnTermFlagsOn(fd_tty)) == -1){
                    perror("tcsetattr error");
                }else{
                    close(fd_tty);
                    fclose(fp_tty);
                    printf("\33[2K\r");
                	return 1;
                }
            }
        }
    } 
    return 0;
}

int turnTermFlagsOff(int fd){
	struct termios tempStruct;
    if ((tcgetattr(fd, &tempStruct)) == -1) {
        perror("tcgetattr error");
        return -1;
    }else{
        tempStruct.c_lflag &= ~ECHO + ~ICANON;
        return tcsetattr(fd, TCSANOW, &tempStruct);
    }
}

int turnTermFlagsOn(int fd){
	struct termios tempStruct;
    if ((tcgetattr(fd, &tempStruct)) == -1) {
        perror("tcgetattr error");
        return -1;
    }else{
        tempStruct.c_lflag |= ECHO + ICANON;
        return tcsetattr(fd, TCSANOW, &tempStruct);
    }
}

void SIGhandler(int sig)
{
    //signal(sig,SIG_IGN);
    int fd_tty;
    if ((fd_tty = open("/dev/tty", O_RDWR)) == -1) {
        perror("tty open error");
        exit(1);
    }
    turnTermFlagsOn(fd_tty);
    close(fd_tty);
    exit(sig);
}

int countBytes(char * line)
{
    int result = 0;
    int i;
    for (i = 0; line[i] != NULL; i++) {
        result++;
    }
    return result;
}
